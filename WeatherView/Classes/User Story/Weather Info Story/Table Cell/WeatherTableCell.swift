//
//  WeatherTableCell.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class WeatherTableCell: UITableViewCell {
    
    // MARK: - Outlet
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    // MARK: - Lifecyle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cityNameLabel.text = nil
        dateLabel.text = nil
        temperatureLabel.text = nil
    }
    
    // MARK: - Configure
    
    func configure(from viewModel: WeatherInfoViewModel) {
        self.cityNameLabel.text = viewModel.cityName
        self.dateLabel.text = viewModel.date.description
        self.temperatureLabel.text = String(format: "%.1f", viewModel.temperature)
    }
    
}
