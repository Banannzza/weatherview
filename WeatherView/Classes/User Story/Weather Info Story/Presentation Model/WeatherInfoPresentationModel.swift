//
//  WeatherInfoPresentationModel.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class WeatherInfoPresentationModel {
    
    // MARK: - Property
    
    var viewModel = [WeatherInfoViewModel]()
    var availableCharacterSet: CharacterSet {
        let acceptableCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz- "
        return CharacterSet(charactersIn: acceptableCharacters).inverted
    }
    weak var delegate: WeatherInfoPresentationModelDelegate?
    private var timer: Timer?
    
    // MARK: - Methods
    
    
    func obtainDefaultData() {
        let box = [12, 45, 15, 40, 10]
        ServiceLayer.shared.weatherService.mainPrediction(for: box) { [weak self] (response) in
            guard let strongSelf = self else { return }
            switch response {
            case .fail(_):
                break
            case .success(let predictions):
                strongSelf.viewModel = predictions.map { WeatherInfoViewModel(weather: $0) }
                strongSelf.delegate?.viewModelsDidLoad()
            }
        }
    }
    
    private func obtainDataForCity(_ name: String) {
        ServiceLayer.shared.weatherService.mainPrediction(for: name) { [weak self] (response) in
            guard let strongSelf = self else { return }
            switch response {
            case .fail(_):
                break
            case .success(let prediction):
                strongSelf.viewModel = [WeatherInfoViewModel(weather: prediction)]
                strongSelf.delegate?.viewModelsDidLoad()
            }
        }
    }
    
    @objc private func timerTick(_ cityName: String) {
        if cityName.count > 0 {
            self.obtainDataForCity(cityName)
        }
        else {
            self.obtainDefaultData()
        }
    }
    
    func obtain(for name: String) {
        timer?.invalidate()
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.timerTick(name)
            }
        } else {
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timerTick), userInfo: name, repeats: false)
        }
    }
    
}

// MARK: - WeatherInfoPresentationModelDelegate

protocol WeatherInfoPresentationModelDelegate: AnyObject {
    func viewModelsDidLoad()
}
