//
//  WeatherInfoViewModel.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct WeatherInfoViewModel {
    
    let cityName: String
    let date: String
    let temperature: Double
    
    init(weather: Weather) {
        self.cityName = weather.name
        self.date = WeatherInfoViewModel.timestampToDate(weather.timestamp)
        self.temperature = weather.temperature.temperature
    }
    
    private static func timestampToDate(_ timestamp: Double) -> String {
        let dateformatter = DateFormatter()
        
        dateformatter.dateStyle = .short
        
        dateformatter.timeStyle = .short
        
        return dateformatter.string(from: Date(timeIntervalSince1970: timestamp))
    }

}
