//
//  WeatherInfoViewController.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class WeatherInfoViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - Property
    
    private let presentationModel: WeatherInfoPresentationModel
    
    // MARK: - Lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.presentationModel = WeatherInfoPresentationModel()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.presentationModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.presentationModel = WeatherInfoPresentationModel()
        super.init(coder: aDecoder)
        self.presentationModel.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.presentationModel.obtainDefaultData()
    }
    
    // MARK: - Configure
    
    private func configureUI() {
        searchBar.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 40
        tableView.tableFooterView = UIView()
    }
}

// MARK: - UITableViewDataSource

extension WeatherInfoViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: WeatherTableCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! WeatherTableCell
        let prediction = self.presentationModel.viewModel[indexPath.row]
        cell.configure(from: prediction)
        cell.selectionStyle = .none
        return cell
    }
}

// MARK: - UISearchBarDelegate

extension WeatherInfoViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let filtered = text.components(separatedBy: presentationModel.availableCharacterSet).joined()
        return filtered == text
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presentationModel.obtain(for: searchText)
    }
}

// MARK: - WeatherInfoPresentationModelDelegate

extension WeatherInfoViewController: WeatherInfoPresentationModelDelegate {
    func viewModelsDidLoad() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
