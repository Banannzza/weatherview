//
//  APIConstants.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct APIConstatns {
    
    static private let base = "http://api.openweathermap.org/data/2.5"
    static private let appKey = "40d0441604357119fe7bc2caf49251c6"
    
    private init() {}
    
    static func weather(for city: String) -> URL {
        let convertedCity = city.replacingOccurrences(of: " ", with: "%20")
        let fullStr = base + "/weather?q=\(convertedCity)" + "&APPID=\(appKey)"
        return URL(string: fullStr)!
    }
    
    static func weather(for box: [Int]) -> URL {
        let boxString = box.map({ String.init($0) }).joined(separator: ",")
        let fullStr = base + "/box/city?bbox=\(boxString)" + "&APPID=\(appKey)"
        return URL(string: fullStr)!
    }
    
}
