//
//  ServiceResponse.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

enum ServiceResponse<ObjectType> {
    case fail(Error?)
    case success(ObjectType)
}
