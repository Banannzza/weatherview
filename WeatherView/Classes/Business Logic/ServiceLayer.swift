//
//  ServiceLayer.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

final class ServiceLayer {
    
    static let shared = ServiceLayer()
    let weatherService: WeatherServiceProtocol
    
    private init() {
        let transport = WebTransport()
        self.weatherService = WeatherService(transport: transport)
    }
    
}
