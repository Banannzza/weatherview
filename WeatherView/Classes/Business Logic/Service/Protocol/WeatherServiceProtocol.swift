//
//  WeatherServiceProtocol.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol WeatherServiceProtocol {
    
    /// Получение данных о погоде
    ///
    /// - Parameter city: Название города
    /// - Parameter completionHandler: Ответ от сервиса с данными о погоде
    func mainPrediction(for city: String, completionHandler: @escaping (ServiceResponse<Weather>) -> Void)
    
    /// Получение данных о погоде
    ///
    /// - Parameter box: Выбранная область
    /// - Parameter completionHandler: Ответ от сервиса с данными о погоде
    func mainPrediction(for box: [Int], completionHandler: @escaping (ServiceResponse<[Weather]>) -> Void)
    
}
