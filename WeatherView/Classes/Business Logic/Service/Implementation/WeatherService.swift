//
//  WeatherService.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct WeatherService: WeatherServiceProtocol {
    
    private let transport: TransportProtocol
    
    init(transport: TransportProtocol) {
        self.transport = transport
    }
    
    func mainPrediction(for city: String, completionHandler: @escaping (ServiceResponse<Weather>) -> Void) {
        self.transport.request(with: APIConstatns.weather(for: city), jsonCompletionHandler: { json in
            if let json = json, let weather: Weather = ObjectParser.parseObject(fromJson: json).first {
                completionHandler(.success(weather))
            }
            else {
                completionHandler(.fail(nil))
            }
        })
    }
    
    func mainPrediction(for box: [Int], completionHandler: @escaping (ServiceResponse<[Weather]>) -> Void) {
        self.transport.request(with: APIConstatns.weather(for: box), jsonCompletionHandler: { json in
            if let json = json {
                let weather: [Weather] = ObjectParser.parseObject(fromJson: json)
                completionHandler(.success(weather))
            }
            else {
                completionHandler(.fail(nil))
            }
        })
    }
    
}
