//
//  Weather.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct Weather: Decodable {
    
    let name: String
    let timestamp: Double
    let prediction: [WeatherPrediction]
    let temperature: TemperaturePrediction

    enum CodingKeys: String, CodingKey {
        case name
        case timestamp = "dt"
        case prediction = "weather"
        case temperature = "main"
    }
    
}
