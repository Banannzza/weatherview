//
//  WeatherPrediction.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct WeatherPrediction: Decodable {
    
    let id: Int
    let main: String
    let description: String
    let icon: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case main
        case description
        case icon
    }
}
