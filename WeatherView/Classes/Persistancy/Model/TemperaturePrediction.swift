//
//  TemperaturePrediction.swift
//  WeatherView
//
//  Created by Алексей Остапенко on 18/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct TemperaturePrediction: Decodable {
    
    let temperature: Double
    let pressure: Double
    let humidity: Double
    let minTemperature: Double
    let maxTemperature: Double
    
    enum CodingKeys: String, CodingKey {
        case temperature = "temp"
        case pressure
        case humidity
        case minTemperature = "temp_min"
        case maxTemperature = "temp_max"
    }
    
}
